﻿using Microsoft.AspNetCore.Http;
using SportStore.Infrastructure.WebCommon.Helpers;
using SportStore.Models;

namespace SportStore.Infrastructure.WebCommon {
    public class CartStorage : IStorage<CartViewModel> {

        private readonly IHttpContextAccessor _httpContextAccessor;
        public CartStorage(IHttpContextAccessor httpContextAccessor) {
            _httpContextAccessor = httpContextAccessor;
        }

        public void Clear() {
            _httpContextAccessor.HttpContext.Session.Clear();
        }

        public CartViewModel GetItem() {
            CartViewModel cart = _httpContextAccessor.HttpContext.Session.GetJson<CartViewModel>("Cart") ?? new CartViewModel();
            return cart;
        }

        public void Save(CartViewModel item) {
            _httpContextAccessor.HttpContext.Session.SetJson("Cart", item);
        }
    }
}

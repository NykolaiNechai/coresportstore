﻿using Microsoft.AspNetCore.Mvc;
using SportStore.Infrastructure.BusinessLogic;
using SportStore.Models;

namespace SportStore.Infrastructure.WebCommon.Widget {
    public class CartSummaryViewComponent : ViewComponent {
        private readonly IStorage<CartViewModel> _cartStorage;
        private readonly ICartLogic _cartLogic;

        public CartSummaryViewComponent(IStorage<CartViewModel> cartStorage, ICartLogic cartLogic) {
            _cartStorage = cartStorage;
            _cartLogic = cartLogic;
        }

        public IViewComponentResult Invoke() {
            var cart = _cartStorage.GetItem();
            cart.TotalValue = _cartLogic.CalcTotalValue(cart);

            return View(cart);
        }
    }
}

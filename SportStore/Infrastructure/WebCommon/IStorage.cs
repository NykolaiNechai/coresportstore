﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SportStore.Infrastructure.WebCommon {
    public interface IStorage<T> {
        void Save(T item);
        T GetItem();
        void Clear();
    }
}

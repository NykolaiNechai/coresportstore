﻿using Microsoft.AspNetCore.Http;

namespace SportStore.Infrastructure.WebCommon.Helpers {
    public static class UrlExtention {

        public static string PathAndQuery(this HttpRequest httpRequest) {
            var queryString = httpRequest.QueryString.HasValue ? $"{httpRequest.Path}{httpRequest.QueryString}" : httpRequest.Path.ToString();

            return queryString;
        }
    }
}

﻿using Microsoft.AspNetCore.Mvc;
using SportStore.Domain;
using SportStore.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SportStore.Infrastructure.WebCommon.Helpers {
    public class NavigationMenuViewComponent : ViewComponent {

        private readonly IRepository<Product> _productRepository;

        public NavigationMenuViewComponent(IRepository<Product> repository) {
            _productRepository = repository;
        }

        public IViewComponentResult Invoke() {
            ViewBag.SelectedCategory = RouteData?.Values["category"];
            var categories = _productRepository.FilterFields<string>(x => x.Category).Distinct().OrderBy(x => x);
            return View(categories);
        }
    }
}

﻿using Microsoft.EntityFrameworkCore;
using SportStore.Domain;
using SportStore.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SportStore.Infrastructure
{
    public class OrderRepository : IRepository<Order>
    {
        ApplicationDbContext _ctx;

        public OrderRepository(ApplicationDbContext ctx) {
            _ctx = ctx;
        }

        public void Create(Order item) {
            _ctx.AttachRange(item.Lines.Select(l => l.Product));
            _ctx.Orders.Add(item);

            _ctx.SaveChanges();
        }

        public void Delete(int id) {
            throw new NotImplementedException();
        }

        public IQueryable<U> FilterFields<U>(Func<Order, U> selector) {
            return _ctx.Orders.Select(selector).AsQueryable();
        }

        public IQueryable<Order> Filter(Func<Order, bool> selector) {
            return _ctx.Orders.Where(selector).AsQueryable();
        }

        public Order Get(int id) {
            return _ctx.Orders.Find(id);
        }

        public IQueryable<Order> GetAll() {
            return _ctx.Orders
                .Include(o => o.Lines)
                .ThenInclude(l => l.Product)
                .AsQueryable();
        }

        public IQueryable<Order> GetFilteredProduct(string category, int productPage = 1, int pageSize = 4) {
            throw new NotImplementedException();
        }

        public void Update(Order item) {
            _ctx.Entry(item).State = EntityState.Modified;
            _ctx.AttachRange(item.Lines.Select(l => l.Product));
            _ctx.SaveChanges();
        }
    }
}

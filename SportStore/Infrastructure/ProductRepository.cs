﻿using Microsoft.EntityFrameworkCore;
using SportStore.Domain;
using SportStore.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SportStore.Infrastructure {
    public class ProductRepository : IRepository<Product> {

        ApplicationDbContext _ctx;
        public ProductRepository(ApplicationDbContext ctx) {
            _ctx = ctx;
        }

        public void Create(Product item) {
            _ctx.Products.Add(item);
            _ctx.SaveChanges();
        }

        public void Delete(int id) {
            var product = _ctx.Products.Find(id);
            //_ctx.Entry(product).State = EntityState.Deleted;
            _ctx.Products.Remove(product);
            _ctx.SaveChanges();
        }

        public Product Get(int id) => _ctx.Products.Find(id);

        public IQueryable<Product> GetAll() {
            return _ctx.Products;
        }

        public IQueryable<Product> GetFilteredProduct(string category, int productPage = 1, int pageSize = 4) {
            return _ctx.Products
                .Where(p => category == null || p.Category == category)
                .OrderBy(p => p.ProductID)
                .Skip((productPage - 1) * pageSize)
                .Take(pageSize);
        }

        public IQueryable<U> FilterFields<U>(Func<Product, U> selector) {
            return _ctx.Products.Select(selector).AsQueryable();
        }

        public void Update(Product item) {
            _ctx.Entry(item).State = EntityState.Modified;
            _ctx.SaveChanges();
        }

        public IQueryable<Product> Filter(Func<Product, bool> selector) {
            throw new NotImplementedException();
        }
    }
}

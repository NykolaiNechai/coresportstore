﻿using SportStore.Models;
using System.Linq;

namespace SportStore.Infrastructure.BusinessLogic {
    public class CartLogic : ICartLogic {

        public void AddItem(CartViewModel cart, ProductViewModel product, int qty) {
            CartLineViewModel line = cart.Comodities.Where(p => p.Product.ProductID == product.ProductID).FirstOrDefault();
            if (line == null) {
                cart.Comodities.Add(new CartLineViewModel {
                    Product = product,
                    Quantity = qty
                });
            } else {
                line.Quantity += qty;
            }
        }

        public void RemoveItem(CartViewModel cart, ProductViewModel product) {
            cart.Comodities.RemoveAll(l => l.Product.ProductID == product.ProductID);
        }

        public decimal CalcTotalValue(CartViewModel cart) => cart.Comodities.Sum(l => l.Product.Price * l.Quantity);
        
    }
}

﻿using SportStore.Models;

namespace SportStore.Infrastructure.BusinessLogic {
    public interface ICartLogic {
        void AddItem(CartViewModel cart, ProductViewModel product, int qty);
        decimal CalcTotalValue(CartViewModel cart);
        void RemoveItem(CartViewModel cart, ProductViewModel product);
    }
}
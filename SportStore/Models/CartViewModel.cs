﻿using System.Collections.Generic;

namespace SportStore.Models {
    public class CartViewModel {
        public CartViewModel() {
            Comodities = new List<CartLineViewModel>();
        }

        public List<CartLineViewModel> Comodities { get; set; }
        public decimal TotalValue { get; set; }

        public string ReturnUrl { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SportStore.Models {
    public class CartLineViewModel {
        public int CartLineID { get; set; }
        public ProductViewModel Product { get; set; }
        public int Quantity { get; set; }
    }    
}

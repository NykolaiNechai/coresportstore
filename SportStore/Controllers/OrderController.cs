﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SportStore.Domain;
using SportStore.Domain.Entities;
using SportStore.Infrastructure.BusinessLogic;
using SportStore.Infrastructure.WebCommon;
using SportStore.Models;

namespace SportStore.Controllers
{
    public class OrderController : Controller
    {
        readonly ICartLogic _cartLogic;
        readonly IRepository<Order> _orderRepository;
        readonly IStorage<CartViewModel> _cartStorage;
        readonly IMapper _mapper;

        public OrderController(
            ICartLogic cartLogic,
            IRepository<Order> orderRepository,
            IMapper mapper,
            IStorage<CartViewModel> cartStorage) {
            _cartLogic = cartLogic;
            _orderRepository = orderRepository;
            _mapper = mapper;
            _cartStorage = cartStorage;
        }


        [HttpGet]
        public IActionResult Checkout() {
            return View(new OrderViewModel());
        }

        [HttpPost]
        public IActionResult Checkout(OrderViewModel model) {
            if (!_cartStorage.GetItem().Comodities.Any()) {
                ModelState.AddModelError("", "Sorry, cart is empty");
            }

            if (ModelState.IsValid) {
                model.Lines = _cartStorage.GetItem().Comodities;
                var order = _mapper.Map<Order>(model);
                if (model.OrderID == 0) {
                    _orderRepository.Create(order);
                } else {
                    _orderRepository.Update(order);
                }
                return RedirectToAction(nameof(Complited));
            } else {
                return View(model);
            }
        }

        [HttpGet]
        public IActionResult Complited() {
            _cartStorage.Clear();
            return View();
        }

        [Authorize]
        [HttpGet]
        public IActionResult List() {
            var notShippedOrders = _orderRepository.GetAll().Where(o => !o.Shipped);
            var model = _mapper.Map<IEnumerable<OrderViewModel>>(notShippedOrders);

            return View(model);
        }

        [Authorize]
        [HttpPost]
        public IActionResult MarkShipped(int orderId) {
            var order = _orderRepository.Get(orderId);
            if (order != null) {
                order.Shipped = true;
                _orderRepository.Update(order);
            }

            return RedirectToAction(nameof(List));
        }
    }
}
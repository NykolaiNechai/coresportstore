﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SportStore.Domain;
using SportStore.Domain.Entities;
using SportStore.Models;
using SportStore.Utils;

namespace SportStore.Controllers
{

    [Authorize]
    public class AdminController : Controller
    {
        readonly IRepository<Product> _productRepository;
        readonly IMapper _mapper;

        public AdminController(
            IRepository<Product> productRepository,
            IMapper mapper) {
            _productRepository = productRepository;
            _mapper = mapper;
        }

        [HttpGet]
        public IActionResult Index() {
            var modelProducts = _mapper.Map<IEnumerable<ProductViewModel>>(_productRepository.GetAll());
            return View(modelProducts);
        }

        [HttpGet]
        public IActionResult Create() => View(nameof(Edit), new ProductViewModel());

        [HttpGet]
        public IActionResult Edit(int productId) {
            var product = _productRepository.Get(productId);
            var model = _mapper.Map<ProductViewModel>(product);

            return View(model);
        }

        [HttpPost]
        public IActionResult Edit(ProductViewModel model) {

            if (ModelState.IsValid) {
                var product = _mapper.Map<Product>(model);
                if (model.ProductID == 0) {
                    _productRepository.Create(product);
                } else {
                    _productRepository.Update(product);
                }
                TempData["message"] = $"product: '{product.Name}' was saved";
                return RedirectToAction(nameof(Index));
            } else {
                return View(model);
            }
        }

        [HttpPost]
        public IActionResult Delete(int productId) {
            _productRepository.Delete(productId);
            TempData["message"] = $"was deleted";
            return RedirectToAction(nameof(Index));
        }

        [HttpPost]
        public IActionResult SeedDataBase() {
            SeedData.EnsurePopulated(HttpContext.RequestServices);

            return RedirectToAction(nameof(Index));
        }
    }
}
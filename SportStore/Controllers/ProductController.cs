﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using SportStore.Domain;
using SportStore.Domain.Entities;
using SportStore.Infrastructure.BusinessLogic;
using SportStore.Models;
using System.Collections.Generic;
using System.Linq;

namespace SportStore.Controllers
{
    public class ProductController : Controller
    {
        private readonly ICartLogic _cartLogic;
        private readonly IRepository<Product> _productRepository;
        private readonly IMapper _mapper;
        private const int _pageSize = 4;

        public ProductController(ICartLogic cartLogic, IRepository<Product> productRepository, IMapper mapper) {
            _cartLogic = cartLogic;
            _productRepository = productRepository;
            _mapper = mapper;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public IActionResult List(string category, int productPage = 1) {            
            var repoProducts = _productRepository.GetFilteredProduct(category, productPage, _pageSize);
            var modelProducts = _mapper.Map<IEnumerable<ProductViewModel>>(repoProducts);

            var totalProduct = category == null ? _productRepository.GetAll().Count() : modelProducts.Count();

            var viewModel = new ProductListViewModel {
                CurrentCategory = category,
                Products = modelProducts,
                PagingInfo = new PagingInfo {
                    CurrentPage = productPage,
                    ItemsPerPage = _pageSize,
                    TotalItems = totalProduct
                }
            };
            
            return View(viewModel);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using SportStore.Domain;
using SportStore.Domain.Entities;
using SportStore.Infrastructure.BusinessLogic;
using SportStore.Infrastructure.WebCommon;
using SportStore.Infrastructure.WebCommon.Helpers;
using SportStore.Models;

namespace SportStore.Controllers
{
    public class CartController : Controller
    {
        private readonly ICartLogic _cartLogic;
        private readonly IRepository<Product> _productRepository;
        private readonly IStorage<CartViewModel> _cartStorage;
        private readonly IMapper _mapper;
        private const int _pageSize = 4;

        public CartController(ICartLogic cartLogic, IRepository<Product> productRepository, IMapper mapper, IStorage<CartViewModel> cartStorage) {
            _cartLogic = cartLogic;
            _cartStorage = cartStorage;
            _productRepository = productRepository;
            _mapper = mapper;
        }

        [HttpGet]
        public IActionResult Index(string returnUrl) {
            var cart = _cartStorage.GetItem();
            cart.ReturnUrl = returnUrl;
            cart.TotalValue = _cartLogic.CalcTotalValue(cart);

            return View(cart);
        }

        [HttpPost]
        public RedirectToActionResult AddToCart(int productId, string returnUrl) {
            var product = _productRepository.GetAll()
                .FirstOrDefault(p => p.ProductID == productId);
            var productVm = _mapper.Map<ProductViewModel>(product);

            if (product != null) {
                CartViewModel cart = _cartStorage.GetItem();
                _cartLogic.AddItem(cart, productVm, 1);
                _cartStorage.Save(cart);
            }
            return RedirectToAction("Index", new { returnUrl });
        }

        [HttpPost]
        public RedirectToActionResult RemoveFromCart(int productId, string returnUrl) {
            var product = _productRepository.GetAll()
                .FirstOrDefault(p => p.ProductID == productId);
            var productVm = _mapper.Map<ProductViewModel>(product);

            if (product != null) {
                CartViewModel cart = _cartStorage.GetItem();
                _cartLogic.RemoveItem(cart, productVm);
                _cartStorage.Save(cart);
            }
            return RedirectToAction("Index", new { returnUrl });
        }
    }
}
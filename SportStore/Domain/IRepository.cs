﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SportStore.Domain {

    ////TODO: Implement IUnitOfWork
    public interface IRepository<T>
        where T : class {
        IQueryable<T> GetAll();
        T Get(int id);
        void Create(T item);
        void Update(T item);
        void Delete(int id);

        IQueryable<T> GetFilteredProduct(string category, int productPage = 1, int pageSize = 4);
        IQueryable<U> FilterFields<U>(Func<T, U> selector);
        IQueryable<T> Filter(Func<T, bool> selector);
    }
}

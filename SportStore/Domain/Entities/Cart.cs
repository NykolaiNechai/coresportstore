﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SportStore.Domain.Entities {
    public class Cart {
        public List<CartLine> Lines { get; set; }

        public Cart() {
            Lines = new List<CartLine>();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SportStore.Domain;
using SportStore.Domain.Entities;
using SportStore.Infrastructure;
using SportStore.Infrastructure.BusinessLogic;
using SportStore.Infrastructure.WebCommon;
using SportStore.Models;
using SportStore.Utils;
using SportStore.Utils.Mappings;

namespace SportStore {
    public class Startup {

        private readonly IConfiguration _configuration;

        public Startup(IConfiguration configuration) {
            _configuration = configuration;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services) {
            Mapper.Initialize(cfg => {
                cfg.AddProfile<ProductProfile>();
                cfg.AddProfile<OrderProfile>();
                cfg.AddProfile<CartLineProfile>();
            });

            //Mapper.Initialize(cfg => cfg.CreateMap<ProductViewModel, Product>().ReverseMap());
            //Mapper.Initialize(cfg => cfg.AddProfile<OrderProfile>());
            //Mapper.Initialize(cfg => cfg.AddProfile<CartLineProfile>());

            services.AddAutoMapper();

            services.AddDbContext<ApplicationDbContext>(options => {
                options.UseSqlServer(_configuration["Data:SportStoreProducts:ConnectionString"]);
            });

            ////Identity
            services.AddDbContext<AppIdentityDbContext>(options => {
                options.UseSqlServer(_configuration["Data:SportStoreIdentity:ConnectionString"]);
            });
            services
                .AddIdentity<IdentityUser, IdentityRole>()
                .AddEntityFrameworkStores<AppIdentityDbContext>()
                .AddDefaultTokenProviders();

            //services.AddTransient<IRepository<Product>, FakeProductRepository>();
            services.AddTransient<IRepository<Product>, ProductRepository>();
            services.AddTransient<IRepository<Order>, OrderRepository>();
            services.AddTransient<ICartLogic, CartLogic>();
            services.AddTransient<IStorage<CartViewModel>, CartStorage>();

            services.AddMemoryCache();
            services.AddSession();

            services.AddMvc();

            services.AddHttpContextAccessor();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env) {
            if (env.IsDevelopment()) {
                app.UseDeveloperExceptionPage();
                app.UseStatusCodePages();

                //SeedData.EnsurePopulated(app);
                //SeedIdentityData.EnsurePopulated(app);

            } else {
                app.UseExceptionHandler("/Error");
            }
            
            app.UseStaticFiles();
            app.UseSession();
            app.UseAuthentication();

            app.UseMvc(routes => {
                routes.MapRoute(name: "Error", template: "Error",
                    defaults: new { controller = "Error", action = "Error" });

                routes.MapRoute(
                    name: null,
                    template: "{category}/Page{productPage:int}",
                    defaults: new { controller = "Product", action = "List" }
                );

                routes.MapRoute(
                    name: null,
                    template: "Page{productPage:int}",
                    defaults: new { controller = "Product", action = "List", productPage = 1 }
                );

                routes.MapRoute(
                    name: null,
                    template: "{category}",
                    defaults: new { controller = "Product", action = "List", productPage = 1 }
                );

                routes.MapRoute(
                    name: null,
                    template: "",
                    defaults: new { controller = "Product", action = "List", productPage = 1 });

                routes.MapRoute(name: null, template: "{controller}/{action}/{id?}");
            });

            //app.UseMvcWithDefaultRoute();
            //app.Run(async (context) => {
            //    await context.Response.WriteAsync("Hello World!");
            //});

            // Set up data directory
            //string appRoot = env.ContentRootPath;
            //AppDomain.CurrentDomain.SetData("DataDirectory", Path.Combine(appRoot, "App_Data"));
        }
    }
}

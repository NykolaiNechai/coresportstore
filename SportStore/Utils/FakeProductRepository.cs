﻿using SportStore.Domain;
using SportStore.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SportStore.Utils {
    public class FakeProductRepository : IRepository<Product> {
        public void Create(Product item) {
            throw new NotImplementedException();
        }

        public void Delete(int id) {
            throw new NotImplementedException();
        }

        public IQueryable<Product> Filter(Func<Product, bool> selector) {
            throw new NotImplementedException();
        }

        public IQueryable<U> FilterFields<U>(Func<Product, U> selector) {
            throw new NotImplementedException();
        }

        public Product Get(int id) {
            throw new NotImplementedException();
        }

        public IQueryable<Product> GetAll() {
            return this.GetProducts();
        }

        public IQueryable<Product> GetFilteredProduct(string category, int productPage = 1, int pageSize = 4) {
            throw new NotImplementedException();
        }

        public void Update(Product item) {
            throw new NotImplementedException();
        }

        private IQueryable<Product> GetProducts() {
            return new[] {
                new Product{ Name = "Football", Price = 25 },
                new Product{ Name = "Surf board", Price = 179 },
                new Product{ Name = "Running shoes", Price = 95 },
            }.AsQueryable();
        }
    }
}


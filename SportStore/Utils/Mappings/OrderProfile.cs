﻿using AutoMapper;
using SportStore.Domain.Entities;
using SportStore.Models;

namespace SportStore.Utils.Mappings
{
    public class OrderProfile : Profile
    {
        public OrderProfile() {
            CreateMap<OrderViewModel, Order>()
                .ForMember(dest => dest.OrderID, opt => opt.MapFrom(src => src.OrderID))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name))
                .ForMember(dest => dest.Line1, opt => opt.MapFrom(src => src.Line1))
                .ForMember(dest => dest.Line2, opt => opt.MapFrom(src => src.Line2))
                .ForMember(dest => dest.Line3, opt => opt.MapFrom(src => src.Line3))
                .ForMember(dest => dest.City, opt => opt.MapFrom(src => src.City))
                .ForMember(dest => dest.State, opt => opt.MapFrom(src => src.State))
                .ForMember(dest => dest.Zip, opt => opt.MapFrom(src => src.Zip))
                .ForMember(dest => dest.Country, opt => opt.MapFrom(src => src.Country))
                .ForMember(dest => dest.GiftWrap, opt => opt.MapFrom(src => src.GiftWrap))
                .ForMember(dest => dest.Lines, opt => opt.MapFrom(src => src.Lines));

            CreateMap<Order, OrderViewModel>().ReverseMap();
        }
    }
}

﻿using AutoMapper;
using SportStore.Domain.Entities;
using SportStore.Models;
using System.Linq;

namespace SportStore.Utils.Mappings
{
    public class CartLineProfile : Profile
    {
        public CartLineProfile() {
            CreateMap<CartLineViewModel, CartLine>()
                .ForMember(dest => dest.CartLineID, opt => opt.MapFrom(src => src.CartLineID))
                .ForMember(dest => dest.Product, opt => opt.MapFrom(src => src.Product))
                .ForMember(dest => dest.Quantity, opt => opt.MapFrom(src => src.Quantity));

            CreateMap<CartLine, CartLineViewModel>().ReverseMap();
        }
    }
}

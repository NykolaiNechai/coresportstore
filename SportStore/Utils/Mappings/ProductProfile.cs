﻿using AutoMapper;
using SportStore.Domain.Entities;
using SportStore.Models;

namespace SportStore.Utils.Mappings {
    public class ProductProfile : Profile {

        public ProductProfile() {
            CreateMap<Product, ProductViewModel>()
                .ForMember(dest => dest.ProductID, opt => opt.MapFrom(src => src.ProductID))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name))
                .ForMember(dest => dest.Price, opt => opt.MapFrom(src => src.Price))
                .ForMember(dest => dest.Description, opt => opt.MapFrom(src => src.Description))
                .ForMember(dest => dest.Category, opt => opt.MapFrom(src => src.Category));

            CreateMap<ProductViewModel, Product>().ReverseMap();
        }
    }
}

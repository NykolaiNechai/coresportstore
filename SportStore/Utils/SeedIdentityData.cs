﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SportStore.Utils
{
    public static class SeedIdentityData
    {
        const string adminUser = "Admin";
        const string adminPassword = "Secret123$";

        public static async Task EnsurePopulated(UserManager<IdentityUser> userManager) {            
            IdentityUser usr = await userManager.FindByIdAsync(adminUser);
            if (usr == null) {
                usr = new IdentityUser(adminUser);
                await userManager.CreateAsync(usr, adminPassword);
            }
        }
    }
}
